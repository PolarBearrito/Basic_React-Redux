import React from 'react';
import { Link } from 'react-router';
const RRedux = require('react-redux');
import { browserHistory } from 'react-router';



class Homepage extends React.Component
{	
	constructor(props)
	{
		super(props);
		
		this.state = 
		{
			list: []
		}
		
		this.getChildren		= this.getChildren.bind(this);
	}
	
	componentWillMount()
	{
		this.setState
		({
			list:
			[
				{
					id: 0,
					value: "foo"
				},
				
				{
					id: 1,
					value: "bar",
				}	
			]
		});
	}
	
	getChildren()
	{
		return this.state.list.map(
			i =>
			<li key={i.id}>{i.value}</li>
		);
		
	}
	
	render()
	{
		return(
			<div>
				<ul>
				{this.getChildren()}
				</ul>
			</div>
		);
	}
};
Homepage = RRedux.connect(
	
	s => 
	({
	}),
	
	d =>
	({

	})
	
)(Homepage);




export default Homepage;
	