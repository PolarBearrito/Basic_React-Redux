// src/components/AppRoutes.js
import React from 'react';
import { Router, browserHistory } from 'react-router';
import routes from '../routes';
 


export default class AppRouter extends React.Component 
{
	render() 
	{
		return (
			<Router 
					history={this.props.history} 
					routes={routes} 
					onUpdate={() => 
						{
							
						}
					}
			/>
		);
	}
}