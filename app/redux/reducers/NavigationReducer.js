import React from 'react';
import Redux from 'redux';
import { browserHistory } from 'react-router';


export const generateActionCreator = (type, ...keys) => 
{
    return function(...vals)
	{
		let action = {type};
		keys.map( 
			(key, index) => action[keys[index]] = vals[index] 
		);
		return action;
	}
}


const SET_FAQS = 'SET_FAQS'
export const setAboutFAQs 		= generateActionCreator(SET_FAQS, 'FAQs');

export const CHANGE_PAGE = 'CHANGE_PAGE'
export function changePageRoute(page) 
{
	if(page === PAGES.PORTFOLIO)
		window.scroll(0,0);
	
	return function (dispatch) 
	{  
		dispatch(changePage(page));
		browserHistory.push(ROUTES[page]);
		dispatch(INITS[page]);
	}

}

export function changePage(page) 
{
	return {
		type: CHANGE_PAGE,
		page
	}
}



// The Reducer Function
const NavigationReducer = function(state, action) 
{
	if (state === undefined) 
	{
		state = 
		{
			page: ''
		};
	}

	switch(action.type) 
	{
		case 'CHANGE_PAGE':
			return Object.assign({}, state, { page: action.page });
	}
	return state;
}

export default NavigationReducer